package net.iescierva.ramonmr95.p0401automata.util;

import net.iescierva.ramonmr95.p0401automata.models.Automaton;


public class Validator {

    private Automaton automaton;

    public Validator(Automaton automaton) {
        this.automaton = automaton;
    }

    public boolean validate(String num) {
        char charActual;
        int posActual = automaton.getStartingState();
        int posInput;

        for (int i = 0; i < num.length(); i++) {
            charActual = num.charAt(i);

            if (!automaton.getPosibleStates().containsKey(charActual)
                        || automaton.getPosibleStates().get(charActual) == null) {
                return false;
            }
            posInput = automaton.getPosibleStates().get(charActual);

            int value = automaton.getTransitionTable()[posActual][posInput];
            if (value == -1) {
                return false;
            }
            posActual = value;

        }

        for (Integer validPos : automaton.getValidEndingStates()) {
            if (validPos.equals(posActual)) {
                return true;
            }
        }
        return false;
    }


}

