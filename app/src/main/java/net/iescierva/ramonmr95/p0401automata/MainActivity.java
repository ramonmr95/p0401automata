package net.iescierva.ramonmr95.p0401automata;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import net.iescierva.ramonmr95.p0401automata.models.Automaton;
import net.iescierva.ramonmr95.p0401automata.util.Validator;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private EditText etNum;
    private TextView tvValid;
    private Automaton automaton;
    private Validator validator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }


    public void validate(View view) {
        String num = etNum.getText().toString();

        if (validator.validate(num)) {
            tvValid.setTextColor(Color.GREEN);
            tvValid.setText(R.string.valid_format);
        }
        else {
            tvValid.setTextColor(Color.RED);
            tvValid.setText(R.string.invalid_format);
        }

    }

    private void init() {
        tvValid = findViewById(R.id.tvValid);
        etNum = findViewById(R.id.etNum);

        etNum.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                tvValid.setText("");
            }
        });

        initAutomaton();

        validator = new Validator(automaton);
    }

    private void initAutomaton() {
        int[][] transitionTable = new int[][]{{1, 2, -1, 3},
                                              {1, 2, -1, 3},
                                              {-1, 2, 4, 3},
                                              {-1, 3, 4, -1},
                                              {5, 6, -1, -1},
                                              {5, 6, -1, -1},
                                              {-1, 6, -1, -1}};

        int startingState = 0;

        int[] validEndingStates = new int[]{2, 3, 6};

        Map<Character, Integer> posibleStates = initPosibleStates();

        automaton = new Automaton(transitionTable, startingState, validEndingStates, posibleStates);
    }

    private Map<Character, Integer> initPosibleStates() {
        Map<Character, Integer> posibleStates = new HashMap<>();
        posibleStates.put('+', 0);
        posibleStates.put('-', 0);
        posibleStates.put('0', 1);
        posibleStates.put('1', 1);
        posibleStates.put('2', 1);
        posibleStates.put('3', 1);
        posibleStates.put('4', 1);
        posibleStates.put('5', 1);
        posibleStates.put('6', 1);
        posibleStates.put('7', 1);
        posibleStates.put('8', 1);
        posibleStates.put('9', 1);
        posibleStates.put('e', 2);
        posibleStates.put('E', 2);
        posibleStates.put('.', 3);
        posibleStates.put(',', 3);
        return posibleStates;
    }

}
