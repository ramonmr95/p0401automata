package net.iescierva.ramonmr95.p0401automata.models;

import java.util.Map;

public class Automaton {

    private int[][] transitionTable;
    private int[] validEndingStates;
    private int startingState;
    private Map<Character, Integer> posibleStates;

    public Automaton(int[][] transitionTable, int startingState, int[] validEndingStates, Map<Character, Integer> posibleStates) {
        this.transitionTable = transitionTable;
        this.startingState = startingState;
        this.validEndingStates = validEndingStates;
        this.posibleStates = posibleStates;
    }

    public int[][] getTransitionTable() {
        return transitionTable;
    }

    public void setTransitionTable(int[][] transitionTable) {
        this.transitionTable = transitionTable;
    }

    public int[] getValidEndingStates() {
        return validEndingStates;
    }

    public void setValidEndingStates(int[] validEndingStates) {
        this.validEndingStates = validEndingStates;
    }

    public Map<Character, Integer> getPosibleStates() {
        return posibleStates;
    }

    public void setPosibleStates(Map<Character, Integer> posibleStates) {
        this.posibleStates = posibleStates;
    }

    public int getStartingState() {
        return startingState;
    }

    public void setStartingState(int startingState) {
        this.startingState = startingState;
    }
}
